package ru.t1.kharitonova.tm.command.project;

import ru.t1.kharitonova.tm.enumerated.Sort;
import ru.t1.kharitonova.tm.model.Project;
import ru.t1.kharitonova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand{

    @Override
    public String getDescription() {
        return "Show project list.";
    }

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Project> projects = getProjectService().findAll(userId, sort);
        int index = 1;
        for (final Project project: projects){
            if (project == null) continue;
            System.out.println(index + ". " + project.getName() + " : " + project.getStatus());
            index++;
        }
    }

}
