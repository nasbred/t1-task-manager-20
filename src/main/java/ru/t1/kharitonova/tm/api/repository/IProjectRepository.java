package ru.t1.kharitonova.tm.api.repository;

import ru.t1.kharitonova.tm.model.Project;
import ru.t1.kharitonova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project>{
}
