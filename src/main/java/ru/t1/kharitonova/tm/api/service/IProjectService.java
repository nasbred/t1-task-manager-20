package ru.t1.kharitonova.tm.api.service;

import ru.t1.kharitonova.tm.enumerated.Sort;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IUserOwnedService<Project>{

    Project changeProjectStatusByIndex(String userId, Integer index, Status status);

    Project changeProjectStatusById(String userId, String id, Status status);

}
