package ru.t1.kharitonova.tm.api.service;

import ru.t1.kharitonova.tm.api.repository.IUserOwnedRepository;
import ru.t1.kharitonova.tm.model.AbstractUserOwnedModel;
import ru.t1.kharitonova.tm.model.Project;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {

    M create(String userId, String name, String description);

    M updateById(String userId, String id, String name, String description);

    M updateByIndex(String userId, Integer index, String name, String description);

}
