package ru.t1.kharitonova.tm.api.model;

import ru.t1.kharitonova.tm.enumerated.Status;

public interface IHasStatus {
    Status getStatus();

    void setStatus(Status created);
}
